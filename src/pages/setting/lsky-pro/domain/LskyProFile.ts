export interface LskyProFile {

    id: number;


    url: string;

    progress: number;

    name: string;

}
