export interface TocItem {

    /**
     * 目录级别，1~6
     */
    level: number;

    /**
     * 元素ID
     */
    id: string;

    /**
     * 标题
     */
    text: string;

}
