enum ArticleTypeEnum {

    /**
     * Markdown
     */
    MARKDOWN = 1,

    /**
     * 富文本
     */
    RICH_TEXT = 4,

    /**
     * 代码笔记
     */
    CODE= 3,

    /**
     * 表格
     */
    EXCEL = 5,

    /**
     * 旧的EditorJS编辑器
     */
    EDITOR_JS = 2

}

export default ArticleTypeEnum;
