enum PlatformTypeEnum {

    UTOOLS = 'utools',
    DOCKER = 'docker',
    TAURI = 'tauri',
    WEB = 'web'

}

export default PlatformTypeEnum;
