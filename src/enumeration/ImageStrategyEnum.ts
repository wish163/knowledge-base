enum ImageStrategyEnum {

    /**
     * 内置
     */
    INNER = 1,

    /**
     * 图床
     */
    IMAGE = 2,

    /**
     * 兰空图床
     */
    LSKY_PRO = 3

}

export default ImageStrategyEnum;
